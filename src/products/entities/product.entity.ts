import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 64 })
  name: string;

  @Column({ type: 'float' })
  price: number;

  @CreateDateColumn()
  createdAT: Date;

  @UpdateDateColumn()
  updatedAT: Date;

  @DeleteDateColumn()
  deletedAT: Date;
}
